class Authentication < ActiveRecord::Base
  belongs_to :user

  def self.from_omniauth(auth)
    where(:uid => auth["uid"], :provider => auth["provider"]).first
  end

  def self.create_from_omniauth(auth)
    create do |a|
      a.uid = auth['uid']
      a.provider = auth['provider']
      a.raw_data = auth.to_json
    end
  end

end
