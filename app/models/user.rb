class User < ActiveRecord::Base
  has_many :authentications

  def self.from_omniauth(auth)
    authentication = Authentication.from_omniauth(auth)
    if authentication
      authentication.user
    else
      self.create_from_omniauth(auth)
    end
  end

  def self.create_from_omniauth(auth)
    authentication = Authentication.create_from_omniauth(auth)
    user = create!
    authentication.user = user
    user
  end

end
